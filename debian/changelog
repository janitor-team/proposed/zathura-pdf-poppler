zathura-pdf-poppler (0.3.0-2) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Repository.

 -- Debian Janitor <janitor@jelmer.uk>  Fri, 17 Jan 2020 04:07:11 +0000

zathura-pdf-poppler (0.3.0-1) unstable; urgency=medium

  * New upstream release
  * debian/: Bump debhelper compat to 12
  * debian/control: Bump Standards-Version
  * debian/copyright:
    - Bump copyright years
    - Document appstream data license

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 07 Jan 2020 21:27:04 +0100

zathura-pdf-poppler (0.2.9-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Bump Standards-Version.
  * debian/copyright: Bump copyright years.
  * debian/: Convert to meson build system.
  * debian/rules: Use zathura sequence.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 18 Mar 2018 12:45:22 +0100

zathura-pdf-poppler (0.2.8-1) unstable; urgency=medium

  * New upstream release.
  * debian/: Bump debhelper compat to 11.
  * debian/control:
    - Bump B-Ds.
    - Bump Standards-Version.
    - Remove obsolete Breaks+Replaces.
    - Move to salsa.debian.org.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 24 Dec 2017 16:50:24 +0100

zathura-pdf-poppler (0.2.7-1) unstable; urgency=medium

  * New upstream release.
    - Sort source files to make build reproducible. (Closes: #842986)
  * debian/{control,rules,control}: Bump debhelper compat to 10.
  * debian/control:
    - Bump Standards-Version.
    - Update Vcs-Git.

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 11 Jan 2017 23:23:55 +0100

zathura-pdf-poppler (0.2.6-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release.
  * debian/control: Recommend poppler-data.

 -- Sebastian Ramacher <sramacher@debian.org>  Fri, 22 Jan 2016 01:05:50 +0100

zathura-pdf-poppler (0.2.5-1) experimental; urgency=medium

  * Split zathura-pdf-poppler from zathura.
    While 3.0 (quilt) multi tarball packages are nice, they do not integrate
    well with git-buildpackage.

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 21 Dec 2015 20:00:12 +0100
